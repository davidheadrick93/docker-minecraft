FROM ubuntu:20.04

RUN apt update && \ 
    apt upgrade -y && \
    apt install openjdk-8-jre-headless wget -y && \
    wget https://launcher.mojang.com/v1/objects/1b557e7b033b583cd9f66746b7a9ab1ec1673ced/server.jar && \
    java -Xmx1024M -Xms1024M -jar server.jar nogui || true && \
    sed -i 's/false/true/g' eula.txt

CMD ["java", "-Xmx1024M", "-Xms1024M", "-jar", "server.jar", "nogui"]